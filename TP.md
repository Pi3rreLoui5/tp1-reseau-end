# B2-TP1 Réseau
 
##  I Exploration locale en solo

### 1. Affichage d'informations sur la pile TCP/IP locale

*Affichez les infos des cartes réseau de votre PC*
- **interface WiFi**

    ```ipconfig -all```
    
    ***nom*** : Carte réseau sans fil Wi-Fi
    
    ***adresse MAC*** : 70-66-55-68-46-A9
    
    ***adresse IP*** : 10.33.1.246
    
- **interface Ethernet**

    ```ipconfig -all```
    
    ***nom*** : Ethernet
    
    ***adresse MAC*** : D4-5D-64-5A-43-5C
    
    ***adresse IP*** : Média déconnecté donc pas d'adresse IP
    
*Affichez votre gateway*
- **Gateway**

```ipconfig -all```

Passerelle par défaut. . . . . . . . . : 10.33.3.253

**En graphique (GUI : Graphical User Interface)**

*Trouvez comment afficher les informations sur une carte IP (change selon l'OS)*
![](https://i.imgur.com/LCPbZXP.png)

**IP :10.33.1.246**

**MAC : 70-66-55-68-46-A9**

**Gateway : 10.33.3.253**

####  A quoi sert la gateway dans le réseau d'YNOV ?

La gateway dans le réseau Ynov sert a se connecter au réseau Internet.

### 2. Modifications des informations

#### A. Modification d'adresse IP (part 1)

🌞 *Utilisez l'interface graphique de votre OS pour changer d'adresse IP :*

![](https://i.imgur.com/drNEec2.png)
![](https://i.imgur.com/0WCvQWf.png)

#### B. Table ARP

- #### 🌞 Exploration de la table ARP

- depuis la ligne de commande, afficher la table ARP
**cmd :** ```arp - a```

- identifier l'adresse MAC de la passerelle de votre réseau, et expliquer comment vous avez repéré cette adresse MAC spécifiquement

comme je connais la Gateway de mon réseau , c'est a dire 10.33.3.253 et il suffit de lire sur la meme ligne l'adresse MAC : 00-12-00-40-4c-bf.

```
Interface : 10.33.1.246 --- 0x11
  Adresse Internet      Adresse physique      Type
  10.33.2.88            68-3e-26-7c-c3-1a     dynamique
  10.33.3.80            3c-58-c2-9d-98-38     dynamique
  ------------------------------------------------------10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
  ```

  - #### 🌞 Et si on remplissait un peu la table ?

  -  Envoyez des ping vers des IP du même réseau que vous

  - affichez votre table ARP
```arp -a```

```
Interface : 10.33.1.246 --- 0x11
  Adresse Internet      Adresse physique      Type
  10.33.0.7             9c-bc-f0-b6-1b-ed     dynamique
  10.33.0.119           18-56-80-70-9c-48     dynamique
  10.33.0.135           f8-5e-a0-06-40-d2     dynamique
  10.33.0.242           74-4c-a1-51-1e-61     dynamique
  10.33.1.10            b0-7d-64-b1-98-d3     dynamique
  10.33.1.243           34-7d-f6-5a-20-da     dynamique
  10.33.2.8             a4-5e-60-ed-0b-27     dynamique
  10.33.2.81            50-eb-71-d6-4e-8f     dynamique
  10.33.2.88            68-3e-26-7c-c3-1a     dynamique
  10.33.2.196           08-71-90-87-b9-8c     dynamique
  10.33.2.211           08-d2-3e-55-63-1a     dynamique
  10.33.3.80            3c-58-c2-9d-98-38     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.254           00-0e-c4-cd-74-f5     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
  ```
  
  - 🌞 listez les adresses MAC associées aux adresses IP que vous avez ping

J'ai utilisé que trois exemples

| Adresse IP | Adresse MAC |
| -------- | -------- |
|   10.33.2.81   |   50-eb-71-d6-4e-8f   |
|   10.33.3.80   |   3c-58-c2-9d-98-38   |
|   10.33.0.119   |  18-56-80-70-9c-48    |

#### C. nmap

- #### 🌞 Utilisez nmap pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre

**cmd :** ```nmap -sn -PE `10.33.0.0/22```

**cmd :** ```arp -a```

```
Interface : 10.33.1.246 --- 0x11
  Adresse Internet      Adresse physique      Type
  10.33.0.60            e2-ee-36-a5-0b-8a     dynamique
  10.33.0.212           48-2c-a0-c8-f1-dc     dynamique
  10.33.1.23            4c-50-77-dc-a7-1d     dynamique
  10.33.2.63            72-a3-54-59-fe-ec     dynamique
  10.33.2.120           18-56-80-70-9c-48     dynamique
  10.33.2.149           82-2d-4a-c7-24-c4     dynamique
  10.33.2.159           32-b9-7f-73-b4-23     dynamique
  10.33.2.241           9c-6b-72-9b-aa-8b     dynamique
  10.33.3.59            02-47-cd-3d-d4-e9     dynamique
  10.33.3.70            38-f9-d3-8d-69-4b     dynamique
  10.33.3.171           ba-a8-7c-d3-2e-74     dynamique
  10.33.3.189           c2-6f-43-3d-c7-fa     dynamique
  10.33.3.226           5c-3a-45-06-7a-5f     dynamique
  10.33.3.235           cc-f9-e4-c5-a3-32     dynamique
  10.33.3.251           5c-3a-45-06-7a-5f     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
  ```
**Voici les adresses IP avec lesquelles j'ai communiquer.**

#### D. Modification d'adresse IP (part 2)

**cmd :** ```ipconfig```

**Avant changement !**

```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::407a:956d:7205:251%17
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.1.246
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
```

![](https://i.imgur.com/p0VvBJt.png)

**cmd :** ```ipconfig```

**Après changement !**

```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::407a:956d:7205:251%17
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.4
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
```

J'ai trouver une nouvelle adresse IP libre 10.33.0.4

Je prouve que j'ai bien un accés a internet 

```
C:\Users\berti>ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=41 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=47 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=30 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=63 ms TTL=115

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 30ms, Maximum = 63ms, Moyenne = 45ms
```

## II. Exploration locale en duo

### 1. Prérequis

### 2. Câblage

### 3. Modification d'adresse IP

- 🌞 **modifiez l'IP des deux machines pour qu'elles soient dans le même réseau**

![](https://i.imgur.com/4Oxw76q.png)

- 🌞 **vérifiez à l'aide de commandes que vos changements ont pris effet**

```
C:\Users\berti>ipconfig

Configuration IP de Windows


Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::f0a0:6d90:c208:4810%29
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . :
```

l'adresse IP a bien était changer , testons la connexion : 

```
C:\Users\berti>ping 192.168.0.2

Envoi d’une requête 'Ping'  192.168.0.2 avec 32 octets de données :
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.0.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms
```
Et j'affiche ma table arp 

**cmd :** ```arp -a``` 

```
Interface : 192.168.0.1 --- 0x1d
  Adresse Internet      Adresse physique      Type
  169.254.148.87        08-97-98-aa-6d-f3     dynamique
  ------------------------------------------------------192.168.0.2           08-97-98-aa-6d-f3     dynamique
  192.168.0.3           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```
je communique bien avec l'autre machine

### 4. Utilisation d'un des deux comme gateway

🌞 c'est bien l'ordinateur de Romain

```
1     2 ms     1 ms     1 ms  LAPTOP-0PRP7B52 [192.168.0.2]
```
```
C:\Users\berti>tracert google.com

Détermination de l’itinéraire vers google.com [142.250.178.142]
avec un maximum de 30 sauts :

  1     2 ms     1 ms     1 ms  LAPTOP-0PRP7B52 [192.168.0.2]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     8 ms     6 ms    14 ms  192.168.1.1
  4    38 ms    38 ms    34 ms  10.125.51.5
  5    38 ms    44 ms    41 ms  10.125.127.82
  6    46 ms    45 ms    31 ms  10.151.14.50
  7    38 ms    39 ms    44 ms  89.89.101.56
  8    46 ms    38 ms    38 ms  62.34.2.169
  9    40 ms    28 ms    57 ms  be5.cbr01-ntr.net.bbox.fr [212.194.171.137]
 10     *        *       43 ms  62.34.2.56
 11    37 ms    65 ms    41 ms  72.14.204.68
 12    38 ms    28 ms    47 ms  108.170.245.1
 13    50 ms    40 ms    38 ms  142.251.64.131
 14    44 ms    37 ms    45 ms  par21s22-in-f14.1e100.net [142.250.178.142]

Itinéraire déterminé.
```

### 5. Petit chat privé

 🌞 sur le PC serveur avec l'IP 192.168.0.1

```
C:\Users\romai\AppData\Local\Temp\netcat-1.11>nc.exe -l -p 8888
yoooooo
df

fds

f

fdfgg
g
g
stop

g
g
g
g
g
g
g
g
g
gros con
g
```

🌞 sur le PC client avec l'IP 192.168.0.2


🌞 pour aller un peu plus loin

```
C:\Users\berti\OneDrive\Documents\Cours YNOV\B2\TP1-réseau\netcat>nc.exe 192.168.0.2 8888
hh
hooo
```
```
C:\Users\romai\AppData\Local\Temp\netcat-1.11>nc.exe -l -p 8888 192.168.0.1
hh
hooo
^C
```
Je communique avec moi même avec 2 terminaux


```
C:\Users\berti\OneDrive\Documents\Cours YNOV\B2\TP1-réseau\netcat>nc.exe -l -p 8888 127.0.0.1
yo
bonjour
ca va ?

```

```
C:\Users\berti\OneDrive\Documents\Cours YNOV\B2\TP1-réseau\netcat>nc.exe 127.0.0.1 8888
yo
bonjour
ca va ?
```

### 6. Firewall

Pas pu faire le firewall avec un seul pc chez moi 😥

## III. Manipulations d'autres outils/protocoles côté client

### 1. DHCP

🌞Exploration du DHCP, depuis votre PC

**cmd :** ```ipconfig /all```

```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek 8821CE Wireless LAN 802.11ac PCI-E NIC
   Adresse physique . . . . . . . . . . . : 70-66-55-68-46-A9
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::407a:956d:7205:251%17(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.25(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Bail obtenu. . . . . . . . . . . . . . : dimanche 19 septembre 2021 18:41:34
   Bail expirant. . . . . . . . . . . . . : lundi 20 septembre 2021 18:41:34
   Passerelle par défaut. . . . . . . . . : 192.168.0.1
   Serveur DHCP . . . . . . . . . . . . . : 192.168.0.1
   IAID DHCPv6 . . . . . . . . . . . : 91252309
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-26-41-9A-01-D4-5D-64-5A-43-5C
   Serveurs DNS. . .  . . . . . . . . . . : 89.2.0.1
                                       89.2.0.2
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```

Les informations à retenir , 

```
Serveur DHCP . . . . . . . . . . . . . : 192.168.0.1
```
```
Bail expirant. . . . . . . . . . . . . : lundi 20 septembre 2021 18:41:34
```

### 2. DNS

🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur

**cmd :** ```ipconfig /all```

```
Serveurs DNS. . .  . . . . . . . . . . : 89.2.0.1
```

🌞 utiliser, en ligne de commande l'outil nslookup pour faire des requêtes DNS à la main

```
C:\Users\berti>nslookup google.com
Serveur :   ns1.numericable.net
Address:  89.2.0.1

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:805::200e
          172.217.18.206
```

Je n'ai pas pu faire le nslookup avec ```ynov.com``` vu que je suis chez moi 😥

Je vois que l'adresse de google.com correspond bel et bien a l'adresse IP du serveur DNS que mon pc connait.

## IV. Wireshark

[...]